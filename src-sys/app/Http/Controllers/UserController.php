<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Flash;


class UserController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit(User $user)
    {   
        $user = Auth::user();
        return view('users.edit', compact('user'));
    }

    public function update(Request $request, $id)
    { 
     
         $this->validate($request,[
            'email' => 'email',
           // 'password' => 'min:6|confirmed',
            'first_name' => 'string|max:255',
            'last_name' => 'string|max:255',
            'profile_picture' => '',
            'access_roles' => 'string|nullable|max:255',
            'is_active' => 'string|nullable|max:255',
            'position' => 'string|nullable|max:255',
            'department' => 'string|nullable|max:255',
        ]);
        $user = User::find($id);

        
if($request->hasFile('profile_picture')){
            $profile_picture = $request->profile_picture;
            $profile_picture_new_name = time().$profile_picture->getClientOriginalName();
            $profile_picture->move('upload/user', $profile_picture_new_name);
            $user->profile_picture = 'upload/user/'.$profile_picture_new_name;
        }

        

        


       // $user->fill($data);
      //  $user->email = request('email');
    //    $user->password = bcrypt(request('password'));
     //   $user->first_name = request('first_name');
     //   $user->last_name = request('last_name');
       // $user->profile_picture = request('profile_picture');
    //    $user->access_roles = request('access_roles');
     //   $user->is_active = request('is_active');

        $user->save();
        
        Flash::message('Your account has been updated!');
        return redirect()->route('users.edit',['user_id' =>$user->user_id]);
    }
}
