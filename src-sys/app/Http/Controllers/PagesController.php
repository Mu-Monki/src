<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function preloader() {
        return view('preloader');
    }

    public function home() {
        return view('home');
    }

    public function reserveRoom() {
        return view('room.reserve');
    }

    public function login() {
        return view('auth.login');
    }

    public function recaptcha(Request $oRequest) {
        
        /*
        if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
            header('HTTP/1.0 403 Forbidden');
            die('You are not allowed to access this file.');     
        }
        */
        header('Content-type: application/json');
        $sUrl= 'https://www.google.com/recaptcha/api/siteverify';
        $sResponse = $oRequest->response;
        $sSecret = '6Lcg0ooUAAAAAAcs2WgzONgsxjQ43CzLdcgcttMN';
        //$sSecret = env('RECAPTCHA_SECRET_KEY','key');
        return file_get_contents($sUrl . '?secret=' . $sSecret . '&response=' . $sResponse);
    }
}
