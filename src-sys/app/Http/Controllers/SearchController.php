<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SearchController extends Controller
{
    // SEARCH FUNCTION FOR LIVE SEARCHING
    public function action(Request $request) 
    {
        if($request->ajax()) {
            $query = $request->get('query');
            if($query != '') {
                $data = DB::table('rooms')->where('room_name', 'like', '%' .$query.'%')->get();
            }
            else {
                $data = DB::table('rooms')->orderBy('room_name', 'desc')->get();
            }

            if($data) {
                foreach($data as $room) {
                    $output .= '<tr>
                                    <td>'.$room->room_name.'<td>
                                    <td>'.$room->status.'<td>
                                    <td> --- <td>
                                </tr>';
                }
            }
            else {
                $output = '<tr>
                                <td align="center" colspan="3"> NO DATA </td>
                            </tr>';
            }

            $data = array(
                'table_data' => $output
            );

            echo json_encode($data);
        }
    }
}
