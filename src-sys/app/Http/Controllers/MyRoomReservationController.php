<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\RoomReservations;
use App\User;
use DB;
use PDF;

class MyRoomReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $room_reservations = $this->get_room_reservation_data();
        
        
     return view('my_room_reservation.show',compact('room_reservations') )->with('reservation_status');
    }
    public function get_room_reservation_data()
    {
     $room_reservations = RoomReservations::where('reservee_id',Auth::user()->user_id)->paginate(10);
     
     return $room_reservations;
    }
    

    public function pdf(Request $request)
    {
     $pdf = \App::make('dompdf.wrapper');
     $pdf->loadHTML($this->convert_room_reservation_data_to_html());
     $request->session()->put('pdf',$pdf);
     
     return $pdf->stream();
    }

    public function convert_room_reservation_data_to_html()
    {
     $room_reservations = $this->get_room_reservation_data();
     $output = '
     <h3 align="center">My Room Reservation Data</h3>
     <table width="100%" style="border-collapse: collapse; border: 0px;">
      <tr>
    <th style="border: 1px solid; padding:12px;" width="20%">Reservation ID No.</th>
    <th style="border: 1px solid; padding:12px;" width="30%">Room No.</th>
    <th style="border: 1px solid; padding:12px;" width="15%">Name</th>
    <th style="border: 1px solid; padding:12px;" width="15%">ID No. </th>
    <th style="border: 1px solid; padding:12px;" width="20%">Date</th>
    <th style="border: 1px solid; padding:12px;" width="20%">Timeslot</th>
    <th style="border: 1px solid; padding:12px;" width="20%">Status</th>
   
    </tr>
     ';  
     foreach($room_reservations as $room_reservation)
     {
      $output .= '
      <tr>
       <td style="border: 1px solid; padding:12px;">'.$room_reservation->reservation_id.'</td>
       <td style="border: 1px solid; padding:12px;">'.$room_reservation->room->room_num.'</td>
       <td style="border: 1px solid; padding:12px;">'.$room_reservation->users->first_name.'</td>
       <td style="border: 1px solid; padding:12px;">'.$room_reservation->reservee_id.'</td>
       <td style="border: 1px solid; padding:12px;">'.$room_reservation->date_reserved.'</td>
       <td style="border: 1px solid; padding:12px;">'.$room_reservation->reservation_time_start.'-'.$room_reservation->reservation_time_end.'</td>
       <td style="border: 1px solid; padding:12px;">'.$room_reservation->reservation_status->reservation_status.'</td>
     
       </tr>
      ';
     }
     $output .= '</table>';
     return $output;
    }
    public function list(Request $request,  $reservation_id){
        // $room_reservations = RoomReservations::find($reservation_id);
         $room_reservations = $this->get_room_reservation_data2($reservation_id);
         //dd($room_reservations);
        //$room_reservations = RoomReservations::find($reservation_id);
        // dd($room_reservations);
         return view('my_room_reservation.list',compact('room_reservations'))->with('users')->with('rooms');
        // return view('my_room_reservation.list')->with('room_reservations',$room_reservations)->with('users')->with('rooms');
     }

    public function pdf2(Request $request)
    {
        $reservation = new RoomReservations();
        $reservation_id = RoomReservations::find($reservation);
     $pdf = \App::make('dompdf.wrapper');
     $pdf->loadHTML($this->convert_room_reservation_data_to_html2($reservation_id));
     $request->session()->put('pdf',$pdf);
     
     return $pdf->stream();
    }

    public function get_room_reservation_data2($reservation_id)
    {
        
       // $room_reservations = RoomReservations::find($reservation_id);
        $room_reservations = RoomReservations::where('reservee_id',Auth::user()->user_id)->first();
        
        //dd($room_reservations);
     
     return $room_reservations;
    }
    public function convert_room_reservation_data_to_html2($reservation_id)
    {
        
     $room_reservations = $this->get_room_reservation_data2($reservation_id);
     //$room_reservations = RoomReservations::find($reservation_id)->where('reservee_id',Auth::user()->user_id)->first();
     //dd($room_reservations);
     $output = '
     <h3 align="center">My Room Reservation Data</h3>
     <div class="card container" style="width:400px">
     <div class="card-body">
    '; if($room_reservations->reservee_id == Auth::user()->user_id){
       
        $output .= '
     <div>
     <span>
     <strong >reservation_id:  '.$room_reservations->reservation_id.' </strong>
     </span>
     </div>
     <div>
     <strong >Room number:  '.$room_reservations->room->room_num.' </strong>
     </div>
     <div>
     <strong> Room name:  '.$room_reservations->room->room_name.' </strong>
     </div>
     <div>
     <strong> name:  '.$room_reservations->users->first_name.' </strong>
     </div>
     <div>
     <strong>ID:  '.$room_reservations->reservee_id.'  </strong>
     </div>
     <div>
     <strong>Date '.$room_reservations->date_reserved.' </strong>
     </div>
     <div>
     <strong>purpose  '.$room_reservations->reservation_purpose.' </strong>
     </div>
     <div>
     <strong>Timestamp '.$room_reservations->reservation_time_start.' - '.$room_reservations->reservation_time_end.'</strong>
     </div>
     </div>
     </div>
     ';
    
  
 


}
return $output;}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(Request $request) 
    {
        
        $output = '';
        if($request->ajax()) {
            $query = $request->get('query');
            
            if($query != '') {
                $data = DB::table('room_reservations')->join('rooms',function($join){
                    $join->on('rooms.room_id', 'room_reservations.room_id');})->join('users',function($join){
                        $join->on('users.user_id', 'room_reservations.reservee_id');})->where('reservation_status_id', 1)->where('reservee_id', 'like', '%' .$query.'%')->orWhere('room_name', 'like', '%' .$query.'%')->orWhere('reservation_id', 'like', '%' .$query.'%')->orWhere('first_name', 'like', '%' .$query.'%')->orWhere('room_num', 'like', '%' .$query.'%')->orWhere('date_reserved', 'like', '%' .$query.'%')->where('reservation_status_id', 1)
                        ->paginate(10)->setpath('');
                    
                        if( !count($data)){
                            return Response('<tr>
                            <td align="center" colspan="8"> NO DATA </td>
                            </tr>');
                        }
                        
                    }
                   
            else {
                $data = DB::table('room_reservations')->join('rooms',function($join){
                    $join->on('rooms.room_id', 'room_reservations.room_id');})->join('users',function($join){
                        $join->on('users.user_id', 'room_reservations.reservee_id');})->orderBy('reservation_id', 'desc')->where('reservation_status_id', 1)
                        ->paginate(10)->setpath('');
                        if( !count($data)){
                            return Response('<tr>
                            <td align="center" colspan="8"> NO DATA </td>
                            </tr>');
                        }
                    }
            
              if($data) {
                foreach($data as $room_reservations) {
                    $output .= '<tr  id=reservation_'.$room_reservations->reservation_id .'>
                                    <td value = "'. $room_reservations->reservation_id .'"><a href="/pending/list/'.$room_reservations->reservation_id.'">'.$room_reservations->reservation_id.'</a></td>
                                    <td value = "'. $room_reservations->room_num .'">'.$room_reservations->room_num.'</td>
                                    <td value = "'. $room_reservations->room_name .'">'.$room_reservations->room_name.'</td>
                                    <td value = "'. $room_reservations->first_name.'">'.$room_reservations->first_name.'</td>
                                    <td value = "'. $room_reservations->reservee_id .'">'.$room_reservations->reservee_id.'</td>
                                    <td value = "'. $room_reservations->date_reserved .'">'.$room_reservations->date_reserved.'</td>
                                    <td value = "'. $room_reservations->reservation_time_start .'-'.$room_reservations->reservation_time_end.'">'.$room_reservations->reservation_time_start .'-'.$room_reservations->reservation_time_end.'</td>
                                   
                                    <td>
                                    
                                    <a href="#" id="approve" style="padding:9px;" onclick="doAction('.$room_reservations->reservation_id.',' .'2'.')">approve</a>
                                    
                                    <a href="#" id="deny" style="color:Tomato;" onclick="doAction('.$room_reservations->reservation_id.',' .'3'.')">deny</a>
                                    </td>   
                                   

                                </tr> ';
                }
            }
            else {
                $output .= '<tr>
                                <td align="center" colspan="3"> NO DATA </td>
                            </tr>
                            ';
            }
            }
            
            
            //$data = array(
            //    'table_data' => $output
            //);
            //echo json_encode($data);
            //echo $data;
            return Response($output);
        
            //return view('room.reserve')->with('rooms', $data);
            
        }
        public function fetch_data(Request $request){
           
           
            $output = '';
        if($request->ajax()){
            $query = $request->get('query');
            $query2 = $request->get('query2');
            //dd($query);
          if($query != '' && $query2 != '')
          {
           
           $data = DB::table('room_reservations')->join('rooms',function($join){
            $join->on('rooms.room_id', 'room_reservations.room_id');})->join('users',function($join){
                $join->on('users.user_id', 'room_reservations.reservee_id');})
                
             ->whereBetween('date_reserved', array($query, $query2))->where('reservation_status_id', 1)
             ->paginate(10)->setpath('');
             if( !count($data)){
                return Response('<tr>
                <td align="center" colspan="8"> NO DATA </td>
                </tr>');
            }
          }
        
          
          else{
            $data = DB::table('room_reservations')->join('rooms',function($join){
                $join->on('rooms.room_id', 'room_reservations.room_id');})->join('users',function($join){
                    $join->on('users.user_id', 'room_reservations.reservee_id');})->orderBy('date_reserved', 'desc')
                    
                    ->where('reservation_status_id', 1)->whereBetween('date_reserved', array($query, $query2))
                    
                    ->paginate(10)->setpath('');
                    if( !count($data)){
                        return Response('<tr>
                        <td align="center" colspan="8"> NO DATA </td>
                        </tr>');
                    }
          }
          if($data) {
            foreach($data as $room_reservations) {
                $output .= '<tr  id=reservation_'.$room_reservations->reservation_id .'>
                                <td value = "'. $room_reservations->reservation_id .'"><a href="/pending/list/'.$room_reservations->reservation_id.'">'.$room_reservations->reservation_id.'</a></td>
                                <td value = "'. $room_reservations->room_num .'">'.$room_reservations->room_num.'</td>
                                <td value = "'. $room_reservations->room_name .'">'.$room_reservations->room_name.'</td>
                                <td value = "'. $room_reservations->first_name.'">'.$room_reservations->first_name.'</td>
                                <td value = "'. $room_reservations->reservee_id .'">'.$room_reservations->reservee_id.'</td>
                                <td value = "'. $room_reservations->date_reserved .'">'.$room_reservations->date_reserved.'</td>
                                <td value = "'. $room_reservations->reservation_time_start .'-'.$room_reservations->reservation_time_end.'">'.$room_reservations->reservation_time_start .'-'.$room_reservations->reservation_time_end.'</td>
                            
                                <td>
                                <a href="#" id="approve" style="padding:9px;" onclick="doAction('.$room_reservations->reservation_id.',' .'2'.')">approve</a>
    
                                <a href="#" id="deny" style="color:Tomato;" onclick="doAction('.$room_reservations->reservation_id.',' .'3'.')">deny</a>
                                </td>   
    
                            </tr>';
            }
        }
        else {
            $output = '<tr>
                            <td align="center" colspan="3"> NO DATA </td>
                        </tr>';
        }
        }
        
        return Response($output);
         }

    public function doAction(Request $oRequest)
    {
        $room_reservations = RoomReservations::find($oRequest->resId);
        $room_reservations->reservation_status = $oRequest->action;
        $room_reservations->save();
        
        return back();
    }

    public function save(Request $request, $reservation_id){
        $room_reservations = RoomReservations::where('reservee_id',Auth::user()->user_id);
        $room_reservations = RoomReservations::find($reservation_id);
        
        $room_reservations->reservation_status_id = $request->action;
        
        $room_reservations->save();
        $request->session()->put('cancel_reservation', $room_reservations);
        return back();
    }


}
    