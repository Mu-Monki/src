<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RoomReservation;
use App\Room;
use App\Rules\DateTimeRule;
use DateTime;
use Auth;
use DB;

class RoomReservationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms = DB::table('rooms')->leftJoin('room_reservations', function($join) {
                                            $join->on('rooms.room_id', '=', 'room_reservations.room_id');
                                        })
                                    ->leftJoin('room_reservation_status', 'room_reservations.status_id', '=', 'room_reservation_status.status_id')
                                    ->leftJoin('users', 'room_reservations.reservee_id', '=', 'users.user_id')->paginate(5);
        //var_dump($rooms);
        return view('room.reserve')->with('rooms', $rooms);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function process(Request $request) 
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        // VALIDATION RULES FOR FORM
        $this->validate($request, [
            'Date' => ['required', new DateTimeRule($request->input('Time-Start'), $request->input('Time-End'))],
            'Time-End' => 'required',
            'Time-Start' => 'required',
            'room' => 'required'
        ]);

        // ROOM RESERVATION OBJECT
        $reservation = new RoomReservation();
        $reservation->reservation_time_start = $request->input('Time-Start');
        $reservation->reservation_time_end = $request->input('Time-End');
        $reservation->date_reserved = $request->input('Date');
        $reservation->reservee_id = Auth::id();
        $reservation->room_id = DB::table('rooms')->where('room_num', $request->input('room'))->value('room_id');
        
        $request->session()->put('reservation', $reservation);

        return view('/room.room-reservation-form')->with('reservation', $reservation);

    }
    
    // SEARCH FUNCTION FOR LIVE SEARCHING
    public function search(Request $request) {
        $output = '';



        if($request->ajax()) {
            date_default_timezone_set('Asia/Manila');
            $query = $request->get('query');
            $page = $request->get('page');
            $start_dt = null;
            $end_dt = null;
            $now_dt = new DateTime();
            if($request->get('dateStart') != null && $request->get('dateEnd') != null) {
                $start_dt = DateTime::createFromFormat('Y-m-d H:i', $request->get('dateStart'));
                $end_dt = DateTime::createFromFormat('Y-m-d H:i', $request->get('dateEnd'));
            }
            
            var_dump($page);
            var_dump($start_dt);
            var_dump($end_dt);
            var_dump($now_dt);
            //dd($query);
            if($query != '' && $end_dt != null && $start_dt != null) {
                $data = DB::table('rooms')->leftJoin('room_reservations', function($join) {
                                            $join->on('rooms.room_id', '=', 'room_reservations.room_id');
                                        })->whereBetween('room_reservations.reservation_time_start', [$start_dt, $end_dt])
                                        ->leftJoin('reservation_status', 'room_reservations.status_id', '=', 'reservation_status.status_id')
                                        ->leftJoin('users', 'room_reservations.reservee_id', '=', 'users.user_id')
                                        ->where('room_name', 'like', '%' .$query. '%')
                                        ->orWhere('room_num', 'like', '%' .$query. '%' )
                                        ->orWhere('status', 'like', '%' .$query. '%')->orderBy('room_num', 'asc')->paginate(5);
                //dd($data);
            }
            elseif($query != '' && $end_dt == null && $start_dt == null) {
                $data = DB::table('rooms')->leftJoin('room_reservations', function($join) {
                                            $join->on('rooms.room_id', '=', 'room_reservations.room_id');
                                            //$join->on('');
                                        })->where('room_reservations.reservation_time_start', '<=', now())
                                        ->leftJoin('reservation_status', 'room_reservations.status_id', '=', 'reservation_status.status_id')
                                        ->leftJoin('users', 'room_reservations.reservee_id', '=', 'users.user_id')
                                        ->where('room_name', 'like', '%' .$query. '%')
                                        ->orWhere('room_num', 'like', '%' .$query. '%' )
                                        ->orWhere('status', 'like', '%' .$query. '%')->orderBy('room_num', 'asc')->paginate(5);
                                        //dd($data);
                //dd($data);
            }
            else {
                $data = DB::table('rooms')->leftJoin('room_reservations', function($join) {
                                            $join->on('rooms.room_id', '=', 'room_reservations.room_id');
                                            //$join->on('');
                                        })
                                        ->leftJoin('reservation_status', 'room_reservations.status_id', '=', 'reservation_status.status_id')
                                        ->leftJoin('users', 'room_reservations.reservee_id', '=', 'users.user_id')->paginate(5);
            }
            //dd($data);
            

            if($data != null) {
                foreach($data as $room) {
                    $output .= '<tr>
                                    <td id = "room_num" value = "'. $room->room_num .'">'. $room->room_num. '</td>
                                    <td id = "room_name" value = "'. $room->room_name .'">'. $room->room_name. '</td>';
                                    if($room->reservation_status != null) {
                                        $output .= '<td id = "status" value = "'. $room->reservation_status .'" >'. $room->reservation_status. '</td>';
                                    }
                                    else {
                                        $output .= '<td id = "status" value = "available" >available</td>';
                                    }
                                    if($room->first_name != null && $room->last_name != null) {
                                        $output .= '<td id = "reservee_name" value = "'. $room->reservee_id .'" >'. $room->first_name. ' '. $room->last_name. '</td></tr>';
                                    }
                                    else {
                                        $output .= '<td> --- </td>
                                        </tr>';
                                    }
                                    
                }
            }
            else {
                $output .= '<tr>
                                <td align="center" colspan="3"> NO ROOMS FOUND </td>
                            </tr>';
            }

            $output .= '<script>
                        $(function() {
                            $("tbody").find("tr").each(function () {
                                $("td").filter(function() {
                                    return $(this).text() === "pending";
                                }).parent().addClass("disabled");
                            });
                        });
                        </script>';
            //$data = array(
            //    'table_data' => $output
            //);
            //echo json_encode($data);
            //echo $data;
            //var_dump($data);
            //var_dump($output);
            return Response($output);
        }
    }

    // UPDATE STATUS FROM TABLE FUNCTION
    public function update_status(Request $request) {

        //$output = '';
        if($request->ajax()) {
            $date_str = $request->get('Date');
            $start_time_str = $request->get('Time-Start');
            $end_time_str = $request->get('Time-End');

            $start_dt = DateTime::createFromFormat('Y-m-d:H:i', $date_str . ':' . $start_time_str);
            $end_dt = DateTime::createFromFormat('Y-m-d:H:i', $date_str . ':' . $end_time_str);

            var_dump($start_dt);
            var_dump($end_dt);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
