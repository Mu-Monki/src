<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\RoomReservation;
use App\RoomReservationStatus;
use App\User;
use DB;
use Gate;



class PendingController extends Controller
{
    public function update(Request $oRequest){
        if(Gate::allows('isAdminAssistant')){

            $reservation_statuses = RoomReservationStatus::where('reservation_status', 'pending')->join('room_reservations',function($join){
                $join->on('room_reservations.reservation_id', 'reservation_status.reservation_id');})->join('rooms',function($join){
                    $join->on('rooms.room_id', 'room_reservations.room_id');})
                    ->where('room_type','laboratory')->orWhere('room_type','classroom')
                    ->with('users')->paginate(10)->setpath('');
             $data = compact('reservation_statuses');
             
                if($oRequest->ajax()){
                    return response()->json(view('pending.users',$data)->render());
                };
                return view('pending.update',$data);}


            
            // $room_reservations = RoomReservations::where('reservation_status_id', 1)->join('rooms',function($join){
            //     $join->on('rooms.room_id', 'room_reservations.room_id');})->where('room_type','laboratory')->orWhere('room_type','classroom')->with('users')->paginate(10)->setpath('');
            //     $data = compact('room_reservations');
            //     if($oRequest->ajax()){
            //         return response()->json(view('pending.users',$data)->render());
            //     };
            //     return view('pending.update',$data);}
                
       // $from = date('2017-02-02');
       // $to = date('2020-02-02');
       // $from = $oRequest->from;
        //$to = $oRequest->to;
       
        elseif(!Gate::allows('isBuildingAdmin')){
            abort(404,'Sorry, You can do this action');}
       
            $reservation_statuses = RoomReservationStatus::where('reservation_status', 'pending')->join('room_reservations',function($join){
                $join->on('room_reservations.reservation_id', 'reservation_status.reservation_id');})->join('rooms',function($join){
                    $join->on('rooms.room_id', 'room_reservations.room_id');})
            ->where('room_type','common_area')->with('users')->paginate(10)->setpath('');
            $data = compact('reservation_statuses');
            if($oRequest->ajax()){
                return response()->json(view('pending.users',$data)->render());
            }
            
            
     // $data['from'] = $from;
     // $data['to'] = $to;
        /*return view('pending.update')->with('user')->with('room_reservations',RoomReservations::where('reservation_status', null)->get());*/
        return view('pending.update',$data);
    }

    public function doAction(Request $oRequest)
    {
        $room_reservations = RoomReservations::find($oRequest->resId);
        $room_reservations->reservation_status_id = $oRequest->action;
        $room_reservations->save();
        $oRequest->session()->put('Pending_action', $room_reservations);
        return back();
    }

    public function save(Request $request, $reservation_id){

        $room_reservations = RoomReservations::find($reservation_id);

        $room_reservations->reservation_status_id = $request->action;
        
        $room_reservations->save();
        $oRequest->session()->put('Pending_action_', $room_reservations);
        return back();
    }

    

    public function fetch_data(Request $request){
        if(Gate::allows('isAdminAssistant')){
       
        $output = '';
    if($request->ajax()){
        $query = $request->get('query');
        $query2 = $request->get('query2');
        //dd($query);
      if($query != '' && $query2 != '')
      {
       
       $data = DB::table('room_reservations')->join('rooms',function($join){
        $join->on('rooms.room_id', 'room_reservations.room_id');})->join('reservation_status',function($join){
            $join->on('room_reservations.reservation_id', 'reservation_status.reservation_id');})
            ->where('reservation_status.reservation_status', 'pending')->join('users',function($join){
            $join->on('users.user_id', 'room_reservations.reservee_id');})
            ->where('room_type','laboratory')->orWhere('room_type','classroom')
         ->whereBetween('date_reserved', array($query, $query2))->where('reservation_status_id', 1)
         ->paginate(10)->setpath('');
         if( !count($data)){
            return Response('<tr>
            <td align="center" colspan="8"> NO DATA </td>
            </tr>');
        }
      }
    
      
      else{
        $data = DB::table('room_reservations')->join('reservation_status',function($join){
            $join->on('room_reservations.reservation_id', 'reservation_status.reservation_id');})
            ->where('reservation_status.reservation_status', 'pending')->join('room_reservations',function($join){
            $join->on('room_reservations.reservation_id', 'room_reservations.reservation_id');})->join('rooms',function($join){
            $join->on('rooms.room_id', 'room_reservations.room_id');})->join('users',function($join){
                $join->on('users.user_id', 'room_reservations.reservee_id');})->orderBy('date_reserved', 'desc')
                ->where('room_type','laboratory')->orWhere('room_type','classroom')
                ->where('reservation_status_id', 1)->whereBetween('date_reserved', array($query, $query2))
                
                ->paginate(10)->setpath('');
                if( !count($data)){
                    return Response('<tr>
                    <td align="center" colspan="8"> NO DATA </td>
                    </tr>');
                }
      }
      if($data) {
        foreach($data as $room_reservations) {
            $output .= '<tr  id=reservation_'.$room_reservations->reservation_id .'>
                            <td value = "'. $room_reservations->reservation_id .'"><a href="/pending/list/'.$room_reservations->reservation_id.'">'.$room_reservations->reservation_id.'</a></td>
                            <td value = "'. $room_reservations->room_num .'">'.$room_reservations->room_num.'</td>
                            <td value = "'. $room_reservations->room_name .'">'.$room_reservations->room_name.'</td>
                            <td value = "'. $room_reservations->first_name.'">'.$room_reservations->first_name.'</td>
                            <td value = "'. $room_reservations->reservee_id .'">'.$room_reservations->reservee_id.'</td>
                            <td value = "'. $room_reservations->date_reserved .'">'.$room_reservations->date_reserved.'</td>
                            <td value = "'. $room_reservations->reservation_time_start .'-'.$room_reservations->reservation_time_end.'">'.$room_reservations->reservation_time_start .'-'.$room_reservations->reservation_time_end.'</td>
                        
                            <td>
                            <a href="#" id="approve" style="padding:9px;" onclick="doAction('.$room_reservations->reservation_id.',' .'2'.')">approve</a>

                            <a href="#" id="deny" style="color:Tomato;" onclick="doAction('.$room_reservations->reservation_id.',' .'3'.')">deny</a>
                            </td>   

                        </tr>';
        }
    }
    else {
        $output = '<tr>
                        <td align="center" colspan="3"> NO DATA </td>
                    </tr>';
    }
    }
    
    return Response($output);
     }
     elseif(Gate::allows('isBuildingAdmin')){
       
        $output = '';
    if($request->ajax()){
        $query = $request->get('query');
        $query2 = $request->get('query2');
        //dd($query);
      if($query != '' && $query2 != '')
      {
       
       $data = DB::table('room_reservations')->join('rooms',function($join){
        $join->on('rooms.room_id', 'room_reservations.room_id');})->join('users',function($join){
            $join->on('users.user_id', 'room_reservations.reservee_id');})
            ->where('room_type','common_area')
         ->whereBetween('date_reserved', array($query, $query2))->where('reservation_status_id', 1)
         ->paginate(10)->setpath('');
         if( !count($data)){
            return Response('<tr>
            <td align="center" colspan="8"> NO DATA </td>
            </tr>');
        }
      }
    
      
      else{
        $data = DB::table('room_reservations')->join('rooms',function($join){
            $join->on('rooms.room_id', 'room_reservations.room_id');})->join('users',function($join){
                $join->on('users.user_id', 'room_reservations.reservee_id');})->orderBy('date_reserved', 'desc')
                ->where('room_type','common_area')
                ->where('reservation_status_id', 1)->whereBetween('date_reserved', array($query, $query2))
                
                ->paginate(10)->setpath('');
                if( !count($data)){
                    return Response('<tr>
                    <td align="center" colspan="8"> NO DATA </td>
                    </tr>');
                }
      }
      if($data) {
        foreach($data as $room_reservations) {
            $output .= '<tr  id=reservation_'.$room_reservations->reservation_id .'>
                            <td value = "'. $room_reservations->reservation_id .'"><a href="/pending/list/'.$room_reservations->reservation_id.'">'.$room_reservations->reservation_id.'</a></td>
                            <td value = "'. $room_reservations->room_num .'">'.$room_reservations->room_num.'</td>
                            <td value = "'. $room_reservations->room_name .'">'.$room_reservations->room_name.'</td>
                            <td value = "'. $room_reservations->first_name.'">'.$room_reservations->first_name.'</td>
                            <td value = "'. $room_reservations->reservee_id .'">'.$room_reservations->reservee_id.'</td>
                            <td value = "'. $room_reservations->date_reserved .'">'.$room_reservations->date_reserved.'</td>
                            <td value = "'. $room_reservations->reservation_time_start .'-'.$room_reservations->reservation_time_end.'">'.$room_reservations->reservation_time_start .'-'.$room_reservations->reservation_time_end.'</td>
                        
                            <td>
                            <a href="#" id="approve" style="padding:9px;" onclick="doAction('.$room_reservations->reservation_id.',' .'2'.')">approve</a>

                            <a href="#" id="deny" style="color:Tomato;" onclick="doAction('.$room_reservations->reservation_id.',' .'3'.')">deny</a>
                            </td>   

                        </tr>';
        }
    }
    else {
        $output = '<tr>
                        <td align="center" colspan="3"> NO DATA </td>
                    </tr>';
    }
    }
    
    return Response($output);
     }
    }

    public function search(Request $request) 
    {
        $output = '';
        if($request->ajax()) {
            $query = $request->get('query');
            
            if($query != '') {
                $data = DB::table('room_reservations')->join('rooms',function($join){
                    $join->on('rooms.room_id', 'room_reservations.room_id');})->join('users',function($join){
                        $join->on('users.user_id', 'room_reservations.reservee_id');})->where('reservation_status_id', 1)->where('reservee_id', 'like', '%' .$query.'%')->orWhere('room_name', 'like', '%' .$query.'%')->orWhere('reservation_id', 'like', '%' .$query.'%')->orWhere('first_name', 'like', '%' .$query.'%')->orWhere('room_num', 'like', '%' .$query.'%')->orWhere('date_reserved', 'like', '%' .$query.'%')->where('reservation_status_id', 1)
                        ->paginate(10)->setpath('');
                    
                        if( !count($data)){
                            return Response('<tr>
                            <td align="center" colspan="8"> NO DATA </td>
                            </tr>');
                        }
                        
                    }
                   
            else {
                $data = DB::table('room_reservations')->join('rooms',function($join){
                    $join->on('rooms.room_id', 'room_reservations.room_id');})->join('users',function($join){
                        $join->on('users.user_id', 'room_reservations.reservee_id');})->orderBy('reservation_id', 'desc')->where('reservation_status_id', 1)
                        ->paginate(10)->setpath('');
                        if( !count($data)){
                            return Response('<tr>
                            <td align="center" colspan="8"> NO DATA </td>
                            </tr>');
                        }
                    }
            
              if($data) {
                foreach($data as $room_reservations) {
                    $output .= '<tr  id=reservation_'.$room_reservations->reservation_id .'>
                                    <td value = "'. $room_reservations->reservation_id .'"><a href="/pending/list/'.$room_reservations->reservation_id.'">'.$room_reservations->reservation_id.'</a></td>
                                    <td value = "'. $room_reservations->room_num .'">'.$room_reservations->room_num.'</td>
                                    <td value = "'. $room_reservations->room_name .'">'.$room_reservations->room_name.'</td>
                                    <td value = "'. $room_reservations->first_name.'">'.$room_reservations->first_name.'</td>
                                    <td value = "'. $room_reservations->reservee_id .'">'.$room_reservations->reservee_id.'</td>
                                    <td value = "'. $room_reservations->date_reserved .'">'.$room_reservations->date_reserved.'</td>
                                    <td value = "'. $room_reservations->reservation_time_start .'-'.$room_reservations->reservation_time_end.'">'.$room_reservations->reservation_time_start .'-'.$room_reservations->reservation_time_end.'</td>
                                   
                                    <td>
                                    
                                    <a href="#" id="approve" style="padding:9px;" onclick="doAction('.$room_reservations->reservation_id.',' .'2'.')">approve</a>
                                    
                                    <a href="#" id="deny" style="color:Tomato;" onclick="doAction('.$room_reservations->reservation_id.',' .'3'.')">deny</a>
                                    </td>   
                                   

                                </tr> ';
                }
            }
            else {
                $output .= '<tr>
                                <td align="center" colspan="3"> NO DATA </td>
                            </tr>
                            ';
            }
            }
            
            
            //$data = array(
            //    'table_data' => $output
            //);
            //echo json_encode($data);
            //echo $data;
            return Response($output);
            
            //return view('room.reserve')->with('rooms', $data);
            
        }

        public function list(Request $request,  $reservation_status_id){
            $reservation_statuses = RoomReservationStatus::find($reservation_status_id);//all();
           
            $data = compact('reservation_statuses');
            dd($data);
            return view('pending.list',$data)->with('users')->with('rooms')
            ->with('reservation_id');
        }
    }


