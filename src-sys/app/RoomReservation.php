<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use App\User;

class RoomReservation extends Model
{
    // Table Name
    //protected $table = 'room_reservations';
    // Primary Key
    //protected $primaryKey = 'id';
    protected $primaryKey = 'reservation_id';
    protected $table = 'room_reservations';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reservee_id','room_id','reservation_date','reservation_time_start','reservation_time_end','reservation_purpose'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    

    public function users()
    {
        return $this->belongsTo('App\User', 'reservee_id');
    }
    public function room()
    {
        return $this->belongsTo('App\Room', 'room_id');
    }
    public function reservation_id() {
        return $this->hasOne('App\RoomReservationStatus','reservation_id');
    }

}
