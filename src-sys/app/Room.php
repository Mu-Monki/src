<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use App\RoomReservations;

class Room extends Model
{
    protected $primaryKey='room_id';
    protected $table='rooms';
    protected $fillable=['room_num', 'room_name', 'room_type', 'floor', 'capacity', 'building_id', 'status'];

    public function room_reservations()
    {
        return $this->hasMany('App\RoomReservations', 'reservation_id');
    }


    
}
