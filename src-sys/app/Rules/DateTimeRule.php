<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use DateTime;

class DateTimeRule implements Rule
{
    protected $start_time;
    protected $end_time;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($start_str, $end_str)
    {
        $this->start_time = $start_str;
        $this->end_time = $end_str;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        date_default_timezone_set('Asia/Manila');
        $start_dt_obj = DateTime::createFromFormat('Y-m-d:H:i', $value . ':' . $this->start_time);
        $end_dt_obj = DateTime::createFromFormat('Y-m-d:H:i', $value . ':' . $this->end_time);
        
        if(($start_dt_obj > new DateTime()) && ($start_dt_obj < $end_dt_obj)) {
            return true;
        }
        else {
            return false;
        }
    
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'That date/time is invalid. Please choose a valid date/time.';
    }
}
