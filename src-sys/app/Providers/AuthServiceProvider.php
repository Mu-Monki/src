<?php


namespace App\Providers;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);
        $gate->define('isStudent', function($user){
            return $user->access_roles == 'Student';
        });
        $this->registerPolicies($gate);
        $gate->define('isEmployee', function($user){
            return $user->access_roles == 'Employee';
        });
        $this->registerPolicies($gate);
        $gate->define('isSuperAdmin', function($user){
            return $user->access_roles == 'Super User';
        });
        $this->registerPolicies($gate);
        $gate->define('isAdminAssistant', function($user){
            return $user->access_roles == 'admin_assistant';
        });
        $this->registerPolicies($gate);
        $gate->define('isBuildingAdmin', function($user){
            return $user->access_roles == 'Building Administrator';
        });

        /* 
        $this->registerPolicies($gate);
        $gate->define('isVpAcademics', function($user){
            return $user->access_roles == 'Vice President of Academics';
        });
        $this->registerPolicies($gate);
        $gate->define('isHrDepartment', function($user){
            return $user->access_roles == 'HR Department';
        });
        $this->registerPolicies($gate);
        $gate->define('isItDepartment', function($user){
            return $user->access_roles == 'IT Department';
        });
        $this->registerPolicies($gate);
        $gate->define('isFinanceDepartment', function($user){
            return $user->access_roles == 'Finance Department';
        });
        $this->registerPolicies($gate);
        $gate->define('isRegistrarDepartment', function($user){
            return $user->access_roles == 'Registrar Department';
        });
        $this->registerPolicies($gate);
        $gate->define('isOseaDepartment', function($user){
            return $user->access_roles == 'OSEA Department';
        });
        $this->registerPolicies($gate);
        $gate->define('isOsasDepartment', function($user){
            return $user->access_roles == 'OSAS Department';
        });
        $this->registerPolicies($gate);
        $gate->define('isCorpComDepartment', function($user){
            return $user->access_roles == 'CorpCom Department';
        });
        $this->registerPolicies($gate);
        $gate->define('isFacilitiesDepartment', function($user){
            return $user->access_roles == 'Facilities Department';
        });
        $this->registerPolicies($gate);
        $gate->define('isPurchasingDepartment', function($user){
            return $user->access_roles == 'Purchasing Department';
        });
        $this->registerPolicies($gate);
        $gate->define('isAdmissionDepartment', function($user){
            return $user->access_roles == 'Admission Department';
        });*/
        

    }
}