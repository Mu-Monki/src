<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomReservationStatus extends Model
{
    public $table = 'reservation_status';
    protected $primaryKey = 'reservation_status_id';
    protected $fillable = ['reservation_id','reservation_status'];

    public function reservation_id() {
        return $this->belongsTo('App\RoomReservations', 'reservation_id');
    }
    public function users()
    {
        return $this->belongsTo('App\User', 'reservee_id');
    }
    public function room()
    {
        return $this->belongsTo('App\Room', 'room_id');
    }
}

