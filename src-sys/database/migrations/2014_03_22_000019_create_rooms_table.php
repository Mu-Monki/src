<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('room_id');
            $table->integer('room_number')->nullable();
            $table->string('room_name');
            $table->string('room_type');
            $table->integer('floor');
            $table->integer('capacity');
            $table->integer('building_id')->unsigned();
            $table->foreign('building_id')->references('building_id')->on('buildings');
            $table->enum('status',['available','reserved','blocked','unavailable','pending']);
           // $table->softDeletes();
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
