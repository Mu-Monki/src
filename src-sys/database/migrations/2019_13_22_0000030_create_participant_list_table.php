<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participant_list', function (Blueprint $table) {
            $table->integer('reservation_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('participant_list', function($table)
{
           
            $table->foreign('reservation_id')->references('reservation_id')->on('room_reservations');
           
            $table->foreign('user_id')->references('user_id')->on('users');
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participant_list');
    }
}
