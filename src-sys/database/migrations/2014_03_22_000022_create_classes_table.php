<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classes', function (Blueprint $table) {
            $table->increments('class_id');
            $table->string('class_name');
            $table->string('school_year');
            $table->integer('room_id')->unsigned();
            $table->foreign('room_id')->references('room_id')->on('rooms');
            $table->string('professor');
            $table->string('day');
            $table->time('time_start');
            $table->time('time_end');
            $table->boolean('is_active');
            $table->date('start_term_date');
            $table->date('end_term_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classes');
    }
}
