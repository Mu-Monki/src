<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomReservationStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_reservation_status', function (Blueprint $table) {
            $table->increments('status_id');
            $table->enum('description',['pending','reserved','available','unavailable','blocked','cancelled']);
            $table->integer('comment_id')->unsigned();
            $table->foreign('comment_id')->references('comment_id')->on('comments');
            $table->boolean('is_approved')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_reservation_status');
    }
}
