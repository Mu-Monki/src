<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAccessRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_access_roles', function (Blueprint $table) {
            $table->increments('access_role_id');
            $table->enum('ar_description',[
                'Student',
                'Employee',
                'Super User',
                'Building Administrator Head', 'Building Administrator Employee',
                'Administrative Assistant Head', 'Adminisrative Assistant Employee',
                'Vice President of Academics',
                'HR Department Head', 'HR Department Employee',
                'IT Department Head', 'IT Department Employee',
                'Finance Department Head','Finance Department Employee',
                'Registrar Department Head','Registrar Department Employee',
                'OSEA Department Head', 'OSEA Department Employee',
                'OSAS Department Head', 'OSAS Department Employee',
                'CorpCom Department Head', 'CorpCom Department Employee',
                'Facilities Department Head', 'Facilites Department Employee',
                'Purchasing Department Head', 'Purchasing Department Employee',
                'Admission Department Head', 'Admission Department Employee'
            ]);
            //ACADEMICS DEPARTMENT FIELD TO BE UPDATED
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_access_roles');
    }
}
