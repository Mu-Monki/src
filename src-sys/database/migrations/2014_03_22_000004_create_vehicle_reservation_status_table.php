<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleReservationStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_reservation_status', function (Blueprint $table) {
            $table->increments('status_id');
            $table->enum('description',['pending','reserved','available','unavailable','blocked','cancelled']);
            $table->integer('comment_id')->unsigned();
            $table->foreign('comment_id')->references('comment_id')->on('comments');
            $table->boolean('is_approved1')->default(0);
            $table->boolean('is_approved2')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_reservation_status');
    }
}
