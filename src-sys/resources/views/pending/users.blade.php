@extends('inc.navbar')
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="Table_Responsive_v1/vendor/bootstrap/css/bootstrap.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="Table_Responsive_v1/vendor/animate/animate.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="Table_Responsive_v1/vendor/select2/select2.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="Table_Responsive_v1/vendor/perfect-scrollbar/perfect-scrollbar.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="Table_Responsive_v1/css/util.css">
  <link rel="stylesheet" type="text/css" href="Table_Responsive_v1/css/main.css">
  <!--===============================================================================================-->
</head>

<body>
  <div class="limiter">
    <div class="container-table100">
      <div class="wrap-table100">
        <div class="table100">
          <table class="table-hover">
            <thead align='center'>
              <tr class="table100-head">
                <th class="column1">Reservation ID No.</th>
                <th class="column2">Room Name</th>
                <th class="column3">Room No.</th>
                <th class="column4">Name</th>
                <th class="column5">ID No.</th>
                <th class="column6">Date</th>
                <th class="column7">Timeslot</th>
                <th class="column8">Approval Status</th>
              </tr>
            </thead>

            <tbody>
              @foreach($reservation_statuses as $reservation_status)
              <tr id="reservation_{{$reservation_status->reservation_id}}" align='center'>
                <td class="column1">
                  <a href="{{route('pending.list',['reservation_id' => $reservation_status->reservation_id])}}">{{$reservation_status->reservation_id}}</a>
                </td>
                <td class="column2">
                  {{$reservation_status->room->room_name}}
                </td>
                <td class="column3">
                  {{$reservation_status->room->room_num}}
                </td>
                <td class="column4">
                  {{$reservation_status->users->first_name}}
                </td>
                <td class="column5">
                  {{$reservation_status->reservee_id}}
                </td>
                <td class="column6">
                  {{$reservation_status->date_reserved}}
                </td>
                <td class="column7">
                  {{$reservation_status->reservation_time_start}} - {{$reservation_status->reservation_time_end}}
                </td>
                <td class="column8"> @if (Session::has("success"))
                  {{ Session::get("success") }}
                  @endif
                  <a href="#" style='padding:9px;' id="approve" onclick="doAction({{$reservation_status->reservation_id}}, 'reserved')">Approve</a>

                  <a href="#" id="deny" style="color:Tomato;" onclick="doAction({{$reservation_status->reservation_id}}, 'available')">Deny</a>
                </td>
                <div id='test'></div>

                @endforeach
                <br><br><br>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  {{$reservation_statuses->render()}}
  </div>

  <!--===============================================================================================-->
  <script src="Table_Responsive_v1/vendor/jquery/jquery-3.2.1.min.js"></script>
  <!--===============================================================================================-->
  <script src="Table_Responsive_v1/vendor/bootstrap/js/popper.js"></script>
  <script src="Table_Responsive_v1/vendor/bootstrap/js/bootstrap.min.js"></script>
  <!--===============================================================================================-->
  <script src="Table_Responsive_v1/vendor/select2/select2.min.js"></script>
  <!--===============================================================================================-->
  <script src="Table_Responsive_v1/js/main.js"></script>

</body>

</html>