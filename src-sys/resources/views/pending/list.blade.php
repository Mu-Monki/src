@extends('layouts.app')
@extends('inc.navbar')
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif



    {{ csrf_field() }}
    <div class="card container" style="width:400px">
  <div class="card-body">

    <div>
    <span>
    <strong >reservation_id: {{$reservation_statuses->reservation_id}}</strong>
    </span>
    </div>
    <div>
    <strong >Room Name: {{ $reservation_statuses->room->room_name }}</strong>
    </div>
    <div>
    <strong >Room Number: {{ $reservation_statuses->room->room_num }}</strong>
    </div>
    <div>
    <strong> Name: {{ $reservation_statuses->users->first_name }}</strong>
    </div>
    <div>
    <strong>ID: {{ $reservation_statuses->reservee_id }} </strong>
    </div>
    <div>
    <strong>Date {{ $reservation_statuses->date_reserved }}</strong>
    </div>
    <div>
    <strong>Purpose {{ $reservation_statuses->reservation_purpose }}</strong>
    </div>
    <div>
    <strong>Timestamp {{$reservation_statuses->reservation_time_start}} - {{$reservation_statuses->reservation_time_end}}</strong>
    </div>
    <div>
    <a href="#" style='padding:9px;' id="approve" onclick="doAction( 'reserved')">Approve</a>
   
   <a href="#" id="deny" style="color:Tomato;" onclick="doAction( 'available')">Deny</a>
    <div class="card-footer">
    
    <!--<button  class='btn btn-primary' href="#" style='padding:9px;' id="approve" onclick="doAction( 2)">approve</button>
   
   <button class='btn btn-danger' href="#" id="deny" style="" onclick="doAction( 3)">deny</button> -->
   
    </div>
    </div>
    </div>

  


@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script > 

function doAction(action) {
  if (action === 'reserved') {
    approve(action);
  } else {
    deny(action);
  }
}
function cbWrapper(data, funct){
    if($("#myForm", data).length > 0)
        top.location.href="/pending";//redirection
    else
        funct(data);
}

function approve(action) {
  const swalWithBootstrapButtons = Swal.mixin({
    confirmButtonClass: 'btn btn-success',
    cancelButtonClass: 'btn btn-danger',
    buttonsStyling: false,
  })
  
  swalWithBootstrapButtons.fire({
    title: 'Approve Pending Reservation',
    text: "Are you sure you want to approve this reservation?",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes',
    cancelButtonText: 'No',
    reverseButtons: true
  }).then((result) => {
    if (result.value) {
      ajaxRequest(action);
      swalWithBootstrapButtons.fire(
        'Approved!',
        'Your pending reservation has been Approved.',
        'success'
      );
      
    } 
  })
}

function deny(action) {
  const swalWithBootstrapButtons = Swal.mixin({
    confirmButtonClass: 'btn btn-success',
    cancelButtonClass: 'btn btn-danger',
    buttonsStyling: false,
  })
  
  swalWithBootstrapButtons.fire({
    title: 'Deny Pending Reservation',
    text: "Are you sure you want to deny this reservation?",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes',
    cancelButtonText: 'No',
    reverseButtons: true
  }).then((result) => {
    if (result.value) {
      ajaxRequest(action);
      swalWithBootstrapButtons.fire(
        'Denied!',
        'Your pending reservation has been Denied.',
        'success',
        
      );
      
    } 
     })
     }
    


function ajaxRequest(action) {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  
  $.post('/pending/save/{{$reservation_statuses->reservation_id}}', {action: action});
  window.location.replace('/pending');
  console.log(action)

  
  //$('#reservation_' + resId).hide();
}
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.2.6/dist/sweetalert2.all.min.js"></script>  
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.2.6/dist/sweetalert2.min.css">

 