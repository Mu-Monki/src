
@extends('layouts.app')
@extends('inc.navbar')
@section('pagecss')
<html lang="en">
    
<head>
    <meta name="_token" content="{{ csrf_token() }}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Table_Responsive_v1/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Table_Responsive_v1/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Table_Responsive_v1/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Table_Responsive_v1/vendor/perfect-scrollbar/perfect-scrollbar.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Table_Responsive_v1/css/util.css">
    <link rel="stylesheet" type="text/css" href="Table_Responsive_v1/css/main.css">
    <!--===============================================================================================-->
    
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  
    <style>
        form {
            width: 100%;
        }

        .form-inline {
            padding: 98px 96px 11px;
            border-radius: 34px;
        }


        .form-group {
            border-radius: 34px;
            margin-left: 1em;
            margin-left: 1em;
            position:sticky;
        }

        .form-group 
        .pagination {
            display: flex;
            justify-content: center;
        }

        .pagination li {
            display: block;
        }

        .fa-search {
            color: #020101;
            margin-left: 8px;
            margin-right:5px;
            position:sticky-top;
        }

        .fa-calendar {
            color: #020101;
            margin-left: 8px;
            margin-right:5px;
        }
    </style>
 </head>
@endsection

<body>
@section('content')

<h3>Pending Room Reservations</h3>
<br/>
<!--<div class="col-md-5">Pending Data - Total Records - <b><span id="total_records"></span></b></div> -->
<div class='form-inline container'>
        <div class='form-group'>
            {{Form::label('date', 'Select Date: ')}}<i class="fa fa-calendar"></i>
            {{Form::date('date', '', ['class' => 'form-control' ,'id'=>'from_date', 'name'=>'from_date'])}}
        </div>
        <div class='form-group'>
            {{Form::label('date', ' To ')}}<i class="fa fa-calendar"></i>
            {{Form::date('date', '', ['class' => 'form-control','id'=>'to_date', 'name'=>'to_date'])}}
        </div>

        <div class='form-group'>
            {{Form::text('search', '', ['class' => 'form-control', 'id' => 'search','name'=>'search' ,'placeholder' => 'Search...'])}}<i style="margin-left: -30px;" class="fa fa-search"></i>
        </div>
       
    </div>
    <!--<div class="col-md-2">
       <button type="button" name="filter" id="filter" class="btn btn-info btn-sm">Filter</button>
       
      </div> -->

 <div class="limiter">
    <div class="container-table100">
      <div class="wrap-table100">
        <div class="table100">
          <table class="table-hover">
            <thead align='center'>
            <tr class="table100-head">
                <th class="column1">Reservation ID No.</th>
                <th class="column2">Room Name</th>
                <th class="column3">Room No.</th>
                <th class="column4">Name</th>
                <th class="column5">ID No.</th>
                <th class="column6">Date</th>
                <th class="column7">Timeslot</th>
                <th class="column8">Approval Status</th>
              </tr>
            </thead>


<tbody>
   @foreach($reservation_statuses as $reservation_status)
   <tr id="reservation_{{$reservation_status->reservation_id}}" align='center'>
   <td class="column1">
  <a href="{{route('pending.list',['reservation_id' => $reservation_status->reservation_id])}}">{{$reservation_status->reservation_id}}</a> 
   </td>
   <td class="column2">
   {{$reservation_status->room->room_name}}
   </td>
   <td class="column3">
   {{$reservation_status->room->room_num}}
   </td>
   <td class="column4">
   {{$reservation_status->users->first_name}}
   </td>
   <td  class="column5"> 
   {{$reservation_status->reservee_id}}
   </td>
   <td class="column6">
   {{$reservation_status->date_reserved}}
   </td>
   <td class="column7">
   {{$reservation_status->reservation_time_start}} - {{$reservation_status->reservation_time_end}}
   </td>
   <td  class="column8"> @if (Session::has("success"))
   {{ Session::get("success") }}
   @endif
   <a href="#" id="approve" onclick="doAction({{$reservation_status->reservation_status}}, 'reserved')">Approve</a>
   
   <a href="#" id="deny" style="color:Tomato;" onclick="doAction({{$reservation_status->reservation_status}}, 'available')">Deny</a>
   </td>
   
   </tr>
   <div id='test'></div>
   
    @endforeach
    <br><br><br>
   
    </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
 {{$reservation_statuses->render()}}
</div>


</body>

@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.2.6/dist/sweetalert2.all.min.js"></script>  
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.2.6/dist/sweetalert2.min.css">
<script type="text/javascript" src="js/pending.js"></script>

<script>
$(document).on('click','.pagination a', function(e){
    e.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    var route = 'http://localhost:8000/pending';
console.log(page)
    $.ajax({
        url: route,
        data:{page:page},
        type: 'GET',
        dataType: 'json',
        success: function(data){
            console.log(data)
            $('.users').html(data);
        }
        
    });
});
</script>

</html>

