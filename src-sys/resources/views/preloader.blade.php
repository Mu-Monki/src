@extends('layouts.app')
@section('pagecss')
<!DOCTYPE html>
<html lang="en">
   
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <title>Opening Screen</title>

        
        <!-- Styles -->
        <style>
        @import url('https://fonts.googleapis.com/css?family=Roboto');
        * {
            font-family: "Roboto", sans-serif;
            font-weight:  300vw; 
            margin:  0;
        }

        h1 {
            max-width: 100%;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 170vh;
            display: flex;
            justify-content: left;
            align-items: center;
            color:#2E61DB;
            margin: -320px 30px 170px;
        }

        html, body{
            height: 10vh;
            width: 100vw;
            margin: 0;
            display: flex;
            align-items: flex-start;
            justify-content: flex-start;
            background: #FFFFFF;
        }  

        section {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%vw;
            height: 100vh;
            pointer-events: none;
            animation: fadeout .5s linear forwards;
            animation-delay: 8.5s;
        }

        .img-logo {
            max-width:100%;
            float: left;
            position: absolute;
            width: 300px;
            height: 300px;
            top: 15px;
            left: 16px;
            margin: -110px -110px 20px;
        }

        .img-paint {
            max-width: 100%;
            width: 50%;
            position: absolute;
            right: 0px;
            padding-position: right;
            margin: -50px -19px 30px;
        }

        .img-girl {
            max-width: 100%;
            width:40%;
            position: absolute;
            right: 8px;
            padding-position: right;
        }

        .count {
            position: absolute;
            top: 100%;
            left: 30%;
            transform: translate(-30%, -1500%);
            font-size: 1vw;
            color: #fff;
            font-weight: 50;
            mix-blend-mode: difference;
            width: 1000px;
            text-alight: left;
        }
        
        #myDiv {
            display: none;
            text-align: center;
        }
                    
        </style>
    </head>
        @endsection
  
    @section('content')
    <body onload="myFunction()" style="margin:0;">
    <div id="preloader"> </div>
        <div style="display:none;" id ="myDiv">
            <h1>Loading SRC System</h1>
            <br>
            
            <img src="images/logover2.png" class="img-logo">
            <img src="images/paint.png" class="img-paint">
            <img src="images/spalshscreen.png" class="img-girl">
        
            
            <section>
            <div class="count"></div>
            </section>
        </div>
        
    
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

        <!-- Scripts -->    
        <script>
        $(document).ready(function() {
        var count = 0;
        var counter = setInterval(function() {
            if(count < 101) {
            $('.count').text(count + '%');
            $('.loader').css('width' , count + '%');
            count++
            } else {
            clearInterval(counter);
            }
        }, 80)
        })
        </script> 

        <script>
            var myVar;

            function myFunction() {
            myVar = setTimeout(showPage, 3000);
            }

            function showPage() {
            document.getElementById("preloader").style.display = "none";
            document.getElementById("myDiv").style.display = "block";
            }
        </script>  
    </body>
  </html>
  @endsection