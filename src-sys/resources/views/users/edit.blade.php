@extends('layouts.app')




@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{route('users.update',['user_id' =>$user->user_id])}}" method='post' enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ method_field('patch') }}
    <div>
    <span>
   
    <strong>First Name: </strong>
            <input type="text" name='first_name'value="{{ $user->first_name }}" disabled>
           <!-- <strong >First Name: {{ $user->first_name }}</strong>-->
            </span>
            </div>
            <div><strong>Last Name: </strong>
            <input type="text" name='last_name'value="{{ $user->last_name }}" disabled>
           
            </div>
            <div>
            <strong>Email: </strong>
            <input type="text" name='email'value="{{ $user->email }}" disabled>
            
            </div>
            <div>
            <strong>Access Role: </strong>
            <input type="text" name='access_roles'value="{{ $user->access_roles }}" disabled>
            
            </div>
            <div>
            <strong>Status: </strong>
            <input type="text" name='is_active'value="{{ $user->is_active }}" disabled>
           
            </div>
            @can('employee','student')
    <div>
    <strong>Position: </strong>
            <input type="text" name='is_active'value="{{ $user->position }}" disabled>
    
    </div>
    <div>
    <strong>Department: </strong>
            <input type="text" name='is_active'value="{{ $user->department }}" disabled>
    
    </div>
    @endcan
    <div>
            <img src="{{$user->profile_picture}}" alt="{{$user->first_name}}" width='50px' height='50px'>
            <input type="file" name='profile_picture'>
            <button type='submit'>submit</button>
            </form>
            </div>
        

        </form>