@extends('layouts.app')
@extends('inc.navbar')
@extends('inc.footer')

@section('pagecss')


@endsection
@section('content')
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>SRC</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link rel="icon" style="width: 24px;" style="height:24px;" type="image/png" href="./images/logover2.png">

  <!-- Libraries CSS Files -->
  <link href="newbiz_template/animate/animate.min.css" rel="stylesheet">
  <link href="newbiz_template/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="newbiz_template/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="newbiz_template/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="newbiz_template/bootstrap/css/bootstrap.min.css" rel="stylesheet">


  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">
</head>

<body>
  <!--========================== Intro Section ============================-->

  <section id="intro" class="clearfix">
    <div class="intro-info typewriter">
      <h1> Welcome to the SRC System, <span>{{ Auth::user()->first_name }}.</span></h1>
      <br>
      <h4>iACADEMY's School Reservation Centralized System. <br>
        Reserve and manage your reservations anytime, anywhere.</h4>
    </div>
  </section><!-- #intro -->


  <main id="main">
      <!--========================== SUBSYSTEMS ============================-->
    <section id="why-us" class="wow fadeIn">
      <div class="container">
        <header class="section-header">
          <h3>Choose a task for today.</h3>
          <br>
        </header>

        <div class="row row-eq-height justify-content-center">
          <div class="col-lg-6 mb-7">
            <div class="room wow bounceInUp"> 
              <img class="room_pic" src="images/roomsubsystem.png">
              <div class="card-body"> 
                <h5 class="card-title">ROOM</h5>
                <div class="room_buttons">
                <form action='./room-reservations' method='get'>
                    <button type='submit' onclick=>Reserve a Room</button>
                </form>
                <button type='button' onclick=>View Rooms</button>
                <form action='./my-room-reservation' method='get'>
                <button type='submit' onclick=>My Room Reservations</button>
                </form>
                <form action='./pending' method='get'>
                  <button type='submit' onclick=>Pending Room Reservations</button>
                </form>
                <button type='button' onclick=>Approved Room Reservations</button>
                </div>

              </div>
            </div>
          </div>

          <div class="col-lg-6 mb-6">
            <div class="vehicle wow bounceInUp">
                <img class="vehicle_pic" src="images/vehiclesubsystem.png">
              <div class="card-body">
                <h5 class="card-title">VEHICLE</h5>
                <div class="vehicle_buttons">
                  <button type='submit' onclick=>Reserve a Vehicle</button><br>
                  <button type='button' onclick=>View Vehicles</button><br>
                  <button type='button' onclick=>My Vehicle Reservations</button><br>
                  <button type='button' onclick=>Pending Vehicle Reservations</button><br>
                  <button type='button' onclick=>Approved Vehicle Reservations</button><br>
                </div>
                
              </div>
            </div>
          </div>

      </div>
    </section>


  </main>
  <!-- Preloader -->
  <div id="preloader"></div> 

  <!-- JavaScript Libraries -->
  <script src="newbiz_template/jquery/jquery.min.js"></script>
  <script src="newbiz_template/jquery/jquery-migrate.min.js"></script>
  <script src="newbiz_template/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="newbiz_template/easing/easing.min.js"></script>
  <script src="newbiz_template/mobile-nav/mobile-nav.js"></script>
  <script src="newbiz_template/wow/wow.min.js"></script>
  <script src="newbiz_template/waypoints/waypoints.min.js"></script>
  <script src="newbiz_template/owlcarousel/owl.carousel.min.js"></script>
  <script src="newbiz_template/isotope/isotope.pkgd.min.js"></script>
  <script src="newbiz_template/lightbox/js/lightbox.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>
</body>

</html>
