
<!-- Font Awesome Icons -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  
  <style>
      .fa-calendar {
          color: #020101;
          margin-left:20px;
          margin-right:10px;
      }

      .fa-search {
        color: #020101;
        margin-left: 8px;
        margin-right:5px;
      }

      .fa-clock-o {
        color: #020101;
        margin-left: 17px;
       }
  </style>

{!! Form::open(['action' => 'RoomReservationsController@store', 'method' => 'POST']) !!}
    <div class='form-inline'>
        <div class='form-group'>
            {{Form::label('room-label', 'Room: ')}}
            {{Form::text('room', '', ['class' => 'form-control', 'id' => 'room', 'readonly' => 'true'])}}
        </div>  
    </div>
    <div class='form-inline'>
        <div class='form-group'>
            {{Form::label('date', 'Select Date: ')}} <i class="fa fa-calendar"></i>
            {{Form::date('Date', '', ['class' => 'form-control', 'id' => 'date'])}}
        </div>
        <div class='form-group'>
            {{Form::label('time', 'Select Time: ')}} <i class="fa fa-clock-o"></i>
            {{Form::time('Time-Start', '', ['class' => 'form-control', 'id' => 'time_start'])}}
        <div class='form-group'>
            {{Form::label('to', ' To ')}} <i class="fa fa-clock-o"></i>
            {{Form::time('Time-End', '', ['class' => 'form-control', 'id' => 'time_end'])}}
        </div>
        <div class='form-group'>
            {{Form::text('search', '', ['class' => 'form-control', 'id' => 'search', 'placeholder' => 'search...'])}}<i class="fa fa-search"></i>
        </div>
        <div class='form-group'>
            {{Form::submit('Submit', ['class' => 'btn btn-primary', 'name' => 'filter'])}}
        </div>
    </div>
    
{!! Form::close() !!}