<!DOCTYPE html>
<html lang="en">

<head>

  <!-- Google Fonts -->
  <link rel="stylesheet" type="text/css" href="./fonts/Roboto/" />

  <!-- Libraries CSS Files -->
  <link href="newbiz_template/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="newbiz_template/animate/animate.min.css" rel="stylesheet">
  <link href="newbiz_template/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="newbiz_template/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="newbiz_template/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">
</head>

<body>

<main id="main">

 <!--========================== Footer ============================-->
 <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-5 col-md-7 footer-info">
           <h3>SRC SYSTEM</h3>
            <p>SRC System known as School Reservation Centralized System is a reservation portal used to manage room and vehicle reservations within iACADEMY.</p>
          </div>

          <div class="col-lg-6 col-md-7 footer-contact">
            <h4>Contact Us</h4>
            <p>
            iACADEMY Nexus Campus,<br>
            7434 Yakal, Makati,<br>
            1203<br>
              <strong>Phone:</strong> (02) 889 5555<br>
              <strong>Email:</strong> inquire@iacademy.edu.ph<br>
            </p>

            <div class="social-links">
              <a href="https://twitter.com/iACADEMY_EDU" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="https://www.facebook.com/iACADEMY/" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="https://www.instagram.com/iacademy_edu/" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="https://www.iacademy.edu.ph/" class="google-plus"><i class="fa fa-google-plus"></i></a>
              <a href="https://ph.linkedin.com/company/iacademy" class="linkedin"><i class="fa fa-linkedin"></i></a>
            </div>

          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright 2019.<strong> School Reservation Centralized System</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=NewBiz
        -->
        Created by <a href="#">Team It's Fine</a>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
   
  </main>


  <!-- JavaScript Libraries -->
  <script src="newbiz_template/jquery/jquery.min.js"></script>
  <script src="newbiz_template/jquery/jquery-migrate.min.js"></script>
  <script src="resources/js/bootstrap.bundle.min.js"></script>
  <script src="newbiz_template/easing/easing.min.js"></script>
  <script src="newbiz_template/mobile-nav/mobile-nav.js"></script>
  <script src="newbiz_template/wow/wow.min.js"></script>
  <script src="newbiz_template/waypoints/waypoints.min.js"></script>
  <script src="newbiz_template/owlcarousel/owl.carousel.min.js"></script>
  <script src="newbiz_template/isotope/isotope.pkgd.min.js"></script>
  <script src="newbiz_template/lightbox/js/lightbox.min.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>
</body>

</html>