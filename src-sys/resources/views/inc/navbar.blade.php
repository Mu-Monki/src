<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>SRC Home Page</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  

  <!-- Libraries CSS Files -->
  <link href="newbiz_template/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="newbiz_template/animate/animate.min.css" rel="stylesheet">
  <link href="newbiz_template/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="newbiz_template/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="newbiz_template/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <style>
    .fa-bell {
      color: #020101;
      margin: -6px 0;
    }

    .fa-user-circle-o {
      color: #020101;
      margin: -6px 0;
    }

    .fa-cogs {
      color: #020101;
      margin: -6px 0;
    }

    .mobile-nav a .fa-bell {
      color: #fff;
    }

    .mobile-nav a .fa-user-circle-o {
      color: #fff;
    }

    .mobile-nav a .fa-cogs {
      color: #fff;
    }
  </style>
</head>

<body>
  <!-- Header -->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">

        <!--SRC Logo -->
        <a href="{{ url('/') }}" class="scrollto"><img src="images/logover2.png" alt="SRC logo" class="img-fluid"></a>
      </div>

      <!-- NAVBAR -->
      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li class="active"><a href="#">Home</a></li>
          <li><a href="#">Summary</a></li>
          <li class="drop-down"><a href="#">Subsystems</a>
            <ul>
              <li class="drop-down"><a href="#">Room</a>
                <ul>
                  <li><a href="{{url('./room-reservations')}}">Reserve a Room</a></li>
                  <li><a href="#">View Rooms</a></li>
                  <li><a href="#">My Room Reservations</a></li>
                  
                    <li><a href="./pending">Pending Room Reservations</a></li>
             
                  <li><a href="#">Approved Room Reservations</a></li>
                </ul>
              </li>
        

            <!--Add department head access role for initial approval of vehicle reservations-->
              <li class="drop-down"><a href="#">Vehicle</a>
                <ul>
                  <li><a href="#">Reserve a Vehicle</a></li>
                  <li><a href="#">View Vehicle</a></li>
                  <li><a href="#">My Vehicle Reservations</a></li>
                  <li><a href="#">Pending Vehicle Reservations</a></li>
                  <li><a href="#">Approved Vehicle Reservations</a></li>
                  <li><a href="#">View Trip Schedule</a></li>
                </ul>
              </li>
            </ul>
          </li>
        

          <li><a href="#"><span class="icon"><i class="fa fa-bell fa-2x"></i></span></a></li>

         
          <li class="drop-down"><a href="#"><span class="icon"><i class="fa fa-cogs fa-2x"></i></a>
         
            <ul>
        
              <li class="drop-down"><a href="#">User</a>
                <ul>
                  <li><a href="#">Add User</a></li>
                  <li><a href="#">Edit User</a></li>
                  <li><a href="#">Delete User</a></li>
                </ul>
              </li>
          

              <li class="drop-down"><a href="#">Room</a>
                <ul>
                
                  <li><a href="#">Add Room</a></li>
                  <li><a href="#">Edit Room</a></li>
                  <li><a href="#">Delete Room</a></li>
                  <li><a href="#">Add Building</a></li>
               

                
                  <li><a href="#">Add Class Schedule</a></li>
                  <li><a href="#">Edit Class Schedule</a></li>
                  <li><a href="#">Delete Class Schedule</a></li>
                </ul>
     
              </li>

         
              <li class="drop-down"><a href="#">Vehicle</a>
                <ul>
                  <li><a href="#">Add Vehicle</a></li>
                  <li><a href="#">Edit Vehicle</a></li>
                  <li><a href="#">Delete Vehicle</a></li>
                  <li><a href="#">Add Driver</a></li>
                  <li><a href="#">Edit Driver</a></li>
                  <li><a href="#">Delete Driver</a></li>
                </ul>
              </li>
        
            </ul>
          </li>
         
          <li class="drop-down"><a href="#"><span class="icon"><i class="fa fa-user-circle-o fa-2x"></i></span>
              {{ Auth::user()->first_name  }}
            </a>
            <ul>
              <!-- Authentication Links -->
              @guest
              <li>
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
              </li>

              @if (Route::has('register'))
              <li>
                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
              </li>

              @endif
              @else
              <li>
                <a href="./usersprofile/{{ Auth::user()->user_id}}">View Profile</a>
              </li>
              <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                  {{ __('Logout') }}
                </a>
              </li>
              <li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
              </li>
            </ul>
          </li>
        </ul>
        @endguest
      </nav><!-- .main-nav -->
    </div>
  </header><!-- #header -->


  <!-- JavaScript Libraries -->
  <script src="newbiz_template/jquery/jquery.min.js"></script>
  <script src="newbiz_template/jquery/jquery-migrate.min.js"></script>
  <script src="resources/js/bootstrap.bundle.min.js"></script>
  <script src="newbiz_template/easing/easing.min.js"></script>
  <script src="newbiz_template/mobile-nav/mobile-nav.js"></script>
  <script src="newbiz_template/wow/wow.min.js"></script>
  <script src="newbiz_template/waypoints/waypoints.min.js"></script>
  <script src="newbiz_template/owlcarousel/owl.carousel.min.js"></script>
  <script src="newbiz_template/isotope/isotope.pkgd.min.js"></script>
  <script src="newbiz_template/lightbox/js/lightbox.min.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>
</body>



</html>
