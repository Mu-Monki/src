@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="en">

<head>
  <title>SRC</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="loginv1_template/vendor/bootstrap/css/bootstrap.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="loginv1_template/vendor/animate/animate.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="loginv1_template/vendor/select2/select2.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="loginv1_template/css/util.css">
  <link rel="stylesheet" type="text/css" href="loginv1_template/css/main.css">
  <!--===============================================================================================-->
</head>

<body>

  <div class="limiter">
    <div class="container-login100">
      <div class="wrap-login100">
        <div class="login100-pic js-tilt" data-tilt>
          <img src="images/login_cover.png" alt="IMG">
        </div>

        <form class="login100-form validate-form" method="POST" action="{{ route('login') }}">
          @csrf
          <form class="needs-validation" novalidate>
            <span class="login100-form-logo">
						  <img src="./images/logover2.png" class="img-fluid"/>
            </span>
            <br>
            <span class="login100-form-title">
              Login to the SRC System
            </span>

            <p style="padding-left:2.2rem;">Please use your iACADEMY email.</p>
         
         

            <div class="wrap-input100 validate-input" data-validate="Valid email is required: ex@iacademy.edu.ph">
            <input style="padding: 0 30px 0 33px;" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" aria-describedby="emailHelp" value="{{ old('email') }}" id="email" type="email" name="email" required autofocus placeholder="Email">
              @if ($errors->has('email'))
                  <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
     
            
			  
            </div>

            <div class="wrap-input100 validate-input" data-validate="Password is required">
              <input style="padding: 0 30px 0 33px;" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" type="password" name="password" placeholder="Password" required>
              @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
              @endif
              
            
            </div>

            <div class="flex-sb-m w-full p-t-3 p-b-32">
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="login_check">
                <label class="custom-control-label" for="login_check">Remember Me</label>
              </div>
            </div>

            <div class="container-login100-form-btn">
              <input type="hidden" value="" name="recaptcha_response" id="recaptchaResponse">
              <button class="login100-form-btn" type="submit">
                {{ __('Login') }}
              </button>
            </div>
          </form>
        </form>
      </div>
    </div>
  </div>




  <!--===============================================================================================-->
  <script src="loginv1_template/vendor/jquery/jquery-3.2.1.min.js"></script>
  <!--===============================================================================================-->
  <script src="loginv1_template/vendor/bootstrap/js/popper.js"></script>
  <script src="loginv1_template/vendor/bootstrap/js/bootstrap.min.js"></script>
  <!--===============================================================================================-->
  <script src="loginv1_template/vendor/select2/select2.min.js"></script>
  <!--===============================================================================================-->
  <script src="loginv1_template/vendor/tilt/tilt.jquery.min.js"></script>
  <script>
    $('.js-tilt').tilt({
      scale: 1.1
    })
  </script>
  <!--===============================================================================================-->
  <script src="loginv1_template/js/main.js"></script>
  <!--===============================================================================================-->
  <script>
    $("#checkbox").click(function() {
      console.log('clicked')
    });
  </script>

</body>

</html>
@endsection
<script src="https://www.google.com/recaptcha/api.js?render=6Lcg0ooUAAAAAEHz0oqgUNTL5w-BjV5UWyxXsSD5"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script>
	 
    grecaptcha.ready(function() {
		  grecaptcha.execute('6Lcg0ooUAAAAAEHz0oqgUNTL5w-BjV5UWyxXsSD5', {action: 'login'}).then(function(token) {
			$.ajaxSetup({
		  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
			$.post('/recaptcha', {response:token}, function (data) {
				console.log(data);
			});
		  });
	  });
	</script>
