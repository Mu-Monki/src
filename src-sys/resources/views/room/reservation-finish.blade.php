@extends('layouts.app')
@section('pagecss')
@endsection
@section('content')
    <div class="card w-100">
        <div class="card-body">
            <h5 class="card-title" align='center'>Finished</h5>
            <p class="card-text" align='center'>
                Your room reservation has been requested.
                Please wait for confirmation.
                Thank you for using SRC.
            </p>
            <a href="/home" class="btn btn-primary" text-align='center'>Ok</a>
        </div>
    </div>
@endsection