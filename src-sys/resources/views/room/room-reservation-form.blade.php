@extends('layouts.app')
@section('pagecss')

@endsection
@section('content')
    {!! Form::open(['action' => 'RoomReservationFormController@store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
    <div class='form-horizontal'>
            <div class='form-group'>
                    {{Form::label('BuildingName', 'Building Name: ', ['class' => 'control-label'])}}
                    {{Form::text('building-name', DB::table('buildings')->where('building_id', (DB::table('rooms')->where('room_id', $reservation->room_id)->value('building_id')))->value('building_name'), ['class' => 'form-control', 'id' => 'bulding-name', 'readonly' => 'true'])}}
            </div> 
            <div class='form-group'>
                    {{Form::label('RoomNo', 'Room No: ', ['class' => 'control-label'])}}
                    {{Form::text('room-no', DB::table('rooms')->where('room_id', $reservation->room_id)->value('room_num'), ['class' => 'form-control', 'id' => 'room-no', 'readonly' => 'true'])}}
            </div> 
            <div class='form-group'>
                    {{Form::label('Date', 'Date: ', ['class' => 'control-label'])}}
                    {{Form::text('date', $reservation->date_reserved, ['class' => 'form-control', 'id' => 'reservation-id', 'readonly' => 'true'])}}
            </div>
            <div class='form-group'>
                    {{Form::label('Timeslot', 'Timeslot: ', ['class' => 'control-label'])}}
                    {{Form::text('timeslot', $reservation->reservation_time_start.' to '.$reservation->reservation_time_end, ['class' => 'form-control', 'id' => 'reservation-timeslot', 'readonly' => 'true'])}}
            </div>
            <div class='form-group'>
                    {{Form::label('Reservee', 'Reservee Name: ', ['class' => 'control-label'])}}
                    {{Form::text('reservee', DB::table('users')->where('user_id', $reservation->reservee_id)->value('first_name').' '.DB::table('users')->where('user_id', $reservation->reservee_id)->value('last_name'), ['class' => 'form-control', 'id' => 'reservee-name', 'readonly' => 'true'])}}
            </div>
            <div class='form-group'>
                    {{Form::label('ReserveeId', 'Reservee ID Num: ', ['class' => 'control-label'])}}
                    {{Form::text('reservee-id', $reservation->reservee_id, ['class' => 'form-control', 'id' => 'reservee-id', 'readonly' => 'true'])}}
            </div>
            <div class='form-group'>
                    {{Form::label('Purpose', 'Purpose: ', ['class' => 'control-label'])}}
                    {{Form::text('purpose', '', ['class' => 'form-control', 'id' => 'purpose'])}}
            </div>
            <div class='form-group'>
                    {{Form::label('Participants', 'Paticipants: ', ['class' => 'control-label'])}}
                    {{Form::text('participants', '', ['class' => 'form-control', 'id' => 'participants'])}}
            </div>
            <div class='form-group'>
                {{Form::submit('Submit', ['class' => 'btn btn-primary', 'name' => 'submit'])}}
            </div>
            
    </div>
    {!! Form::close() !!}
@endsection