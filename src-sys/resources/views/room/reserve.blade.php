@extends('layouts.app')
@extends('inc.navbar')
@section('pagecss')
<html lang="en">
<head>
    <meta name="_token" content="{{ csrf_token() }}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Table_Responsive_v1/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Table_Responsive_v1/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Table_Responsive_v1/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Table_Responsive_v1/vendor/perfect-scrollbar/perfect-scrollbar.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Table_Responsive_v1/css/util.css">
    <link rel="stylesheet" type="text/css" href="Table_Responsive_v1/css/main.css">
    <!--===============================================================================================-->
    <style>
        td {
            align-content: center;
            border: 1em;
        }
        form {
            width: 100%;
        }
        .form-inline {
            padding: 1em;
        }
        .form-group {
            margin-left: 1em;
            margin-left: 1em;
        }
        .disabled {
            background-color: #ececec;
            color: #b9b9b9;
        }
        .pagination {
            display: flex;
            justify-content: center;
        }

        .pagination li {
            display: block;
        }
    </style>
</head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script>
        // FOR CLICKING THE TABLE ROWS
        $(function(){

            $('tbody').on('click', 'tr', function(){

                let room_num = $(this).find('td').attr('value');
                let status = $(this).find('#status').text();
                //console.log('outside if');
                //console.log(room_num);
                //console.log(status);
                if(status == 'available') {
                    //console.log('inside if');
                    $('#room').val(room_num);
                }  else {
                    $('#room').val('');
                }
                //let room_num = $(this).find('td').attr('value');
                //$('#room').val(room_num);
                //alert('clicked ' + room_num);
            });
        });

        // AJAX FOR CHANGING VALUES OF TABLE
        $(document).ready(function(){

            //fetch_data();

            function fetch_data(query = '', page, dateStart, dateEnd) {
                //console.log(dateEnd);
                $.ajax({
                    url: '{{ route('reserve.search') }}',
                    method: 'GET',
                    data: {
                            query:query, 
                            page:page, 
                            dateStart:dateStart, 
                            dateEnd:dateEnd
                            },
                    dataType: 'html',
                    success:function(data) {
                        //console.log(data);
                        //alert(data);
                        //alert(data);
                        $('tbody').html(data);
                        //alert('success');
                    }
                })
            }
            // CALLS AJAX METHOD ON KEYPRESS
            $(document).on('keyup', '#search', function(){
                let query = $(this).val();
                let page = $('.page-item.active span').text();
                let date = $('#date').val();
                let timeStart = $('#time_start').val();
                let timeEnd = $('#time_end').val();
                let startDt;
                let endDt;
                if(checkTime(timeStart) && checkDate(date)) {
                    startDt = date + " " + timeStart;
                } else {
                    startDt = '';
                }
                if(checkTime(timeEnd) && checkDate(date)) {
                    endDt = date + " " + timeEnd;
                } else {
                    endDt = '';
                }
                console.log(page);
                console.log(query);
                console.log('START: ' + startDt);
                console.log('END: ' + endDt);
                fetch_data(query, page, startDt, endDt);
            });

            // CHANGING STATUS ON DATE AND TIME FIELD
            /*
            let userEvent() {

            }
            $(document).on()*/
        });

        $(function(){
            $("tbody").find("tr").each(function () {
                    $("td").filter(function() {
                        return $(this).text() === "pending";
                    }).parent().addClass("disabled");
            });
        });
        
        // AJAX FOR UPDATING STATUS FIELD
        
        $(document).ready(function() {
            /*
            function update_table(date='', time_start='', time_end='') {
                $.ajax({
                    url: '{{ route('reserve.update_status') }}',
                    method:,
                    data:,
                    dataType:,
                    success:function(data) {

                    }
                })
            }*/


            // STORES DATE AND TIMES FORM REQUEST INSIDE VARIABLES
            $(document).on('keyup', '#date', function(){
                let date = $(this).val();
                console.log(date)
                console.log(checkDate(date));
            });
            $(document).on('keyup', '#time_start', function(){
                let timeStart = $(this).val();
                console.log(timeStart);
                console.log(checkTime(timeStart));
            });
            $(document).on('keyup', '#time_end', function(){
                let timeEnd = $(this).val();
                console.log(timeEnd);
                console.log(checkTime(timeEnd));
            });
            
            

        });
        // CLIENT-SIDE VALIDATION FUNCTION FOR DATE STRING
            
        function checkDate(dateStr) {
            //return dateStr.match('(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))' != null);
            return /(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))/.test(dateStr);
        }
        function checkTime(timeStr) {
            //return timeStr.match('' != null);
            return /((0[0-9]|1[0-9]|2[0-3])(:[0-5][0-9]))/.test(timeStr);
        }
    </script>

@endsection
<body>
@section('content')
    <h3>Reserve Room</h3>
    <div class='inputs'>
        @include('inc.datetime_input')
    </div>

    <div class="limiter">
    <div class="container-table100">
      <div class="wrap-table100">
        <div class="table100">
          <table class="table-hover">
            <thead align='center'>
            <tr class="table100-head">
                <th class="column1">Room</th>
                <th class="column1">Status</th>
                <th class="column1">Reservee</th>
            </tr>
        </thead>
        @if($rooms != null)
        <tbody>
            @foreach($rooms as $room)
            <tr>
                    <td class="column1" id='room_num' value='{{$room->room_num}}'>{{$room->room_num}}</td>
                    <td class="column2" id='room_name' value='{{$room->room_name}}'>{{$room->room_name}}</td>
                @if($room->reservation_status != null)
                    <td class="column3" id='status' value='{{$room->status}}'>{{$room->status}}</td>
                @else
                    <td class="column3" id='status' value='available'>available</td>
                @endif
                @if($room->first_name != null)
                    <td class="column4" id='reservee_id' value={{$room->reservee_id}}>{{$room->first_name}} {{$room->last_name}}</td>
                @else
                    <td class="column4">---</td>
                @endif
            </tr>
            @endforeach
            </table>
        </tbody>

        </div>
      </div>
    </div>

    {!! $rooms->links(); !!}
    
        @else
            <h3>No Rooms Found</h3>
        @endif
</body>
@endsection
</html>