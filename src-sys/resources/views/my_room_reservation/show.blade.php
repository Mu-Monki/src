
@extends('layouts.app')
<meta name="csrf-token" content="{{ csrf_token() }}">

@section('content')

<div><strong>My Room Reservations</strong></div>
<br/>
<!--<div class="col-md-5">Pending Data - Total Records - <b><span id="total_records"></span></b></div> -->
<div class='form-inline container'>
        <div class='form-group'>
            {{Form::label('date', 'Select Date: From ')}}
            {{Form::date('date', '', ['class' => 'form-control' ,'id'=>'from_date', 'name'=>'from_date' ])}}
        </div>
        <div class='form-group'>
            {{Form::label('date', 'To ')}}
            {{Form::date('date', '', ['class' => 'form-control','id'=>'to_date', 'name'=>'to_date'])}}
        <div class='form-group'>
            {{Form::text('search', '', ['class' => 'form-control', 'id' => 'search','name'=>'search' ,'placeholder' => 'search...'])}}
        </div>
       
    </div>
  <div><a href="{{ url('dynamic_pdf/pdf') }}" class="btn btn-danger">Convert into PDF</a></div>
    <div class="col-md-2">
    
       <input type="button" value='Download' class="btn btn-info btn-sm">
       <input type="button" value='Sort by:' class="btn btn-info btn-sm">
      </div> 


<div class='users'>

<table  class="container table table-striped" border="1">

    <thead align='center'>
        <th>
        Reservation ID No.
        </th>
        <th>
        Room No.
        </th>
        <th>
        Name
        </th>
        <th>
        ID No.
        </th>
        <th>
        Date
        </th>
        <th>
        Timeslot
        </th>
        <th>
        Status
        </th>
       
    </thead>
<tbody>

    
   @foreach($room_reservations as $room_reservation)
   <tr id="reservation_{{$room_reservation->reservation_id}}" align='center'>
   <td>
  <a href="{{route('my_room_reservation.list',['reservation_id' => $room_reservation->reservation_id])}}">{{$room_reservation->reservation_id}}</a> 
   </td>
   <td>
   {{$room_reservation->room->room_name}}
   </td>
   <td>
   {{$room_reservation->users->first_name}}
   </td>
   <td>
   {{$room_reservation->reservee_id}}
   </td>
   <td>
   {{$room_reservation->date_reserved}}
   </td>
   <td>
   {{$room_reservation->reservation_time_start}} - {{$room_reservation->reservation_time_end}}
   </td>
   <td>
   {{$room_reservation->reservation_status->reservation_status}}
   </td>
   </tr>
   <div id='test'></div>
   
    @endforeach
    <br><br><br>
    
    
    </tbody>
   
    
 </table>
 {{$room_reservations->render()}}
 

</div>

@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.2.6/dist/sweetalert2.all.min.js"></script>  
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.2.6/dist/sweetalert2.min.css">
<script type="text/javascript" src="js/myroomreservation.js"></script>

<script>
$(document).on('click','.pagination a', function(e){
    e.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    var route = 'http://localhost:8000/my-room-reservation';
console.log(page)
    $.ajax({
        url: route,
        data:{page:page},
        type: 'GET',
        dataType: 'json',
        success: function(data){
            console.log(data)
            $('.users').html(data);
        }
        
    });
});
</script>


