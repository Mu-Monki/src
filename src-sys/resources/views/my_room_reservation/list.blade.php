@extends('layouts.app')

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif



    {{ csrf_field() }}
    <div class="card container" style="width:400px">
  <div class="card-body">
    @if($room_reservations->reservee_id == Auth::user()->user_id)
    <div>
    <span>
    <strong >reservation_id: {{ $room_reservations->reservation_id }}</strong>
    </span>
    </div>
    <div>
    <strong >Room number: {{ $room_reservations->room->room_num }}</strong>
    </div>
    <div>
    <strong> Room name: {{ $room_reservations->room->room_name }}</strong>
    </div>
    <div>
    <strong> name: {{ $room_reservations->users->first_name }}</strong>
    </div>
    <div>
    <strong>ID: {{ $room_reservations->reservee_id }} </strong>
    </div>
    <div>
    <strong>Date {{ $room_reservations->date_reserved }}</strong>
    </div>
    <div>
    <strong>purpose {{ $room_reservations->reservation_purpose }}</strong>
    </div>
    <div>
    <strong>Timestamp {{$room_reservations->reservation_time_start}} - {{$room_reservations->reservation_time_end}}</strong>
    </div>
    @if($room_reservations->reservation_status_id == 3)
    <div  class="card-footer">
    {{$room_reservations->reservation_status->reservation_status}}
    
    <button type="sumbit" value='' class='btn btn-primary'>Generate summary report</button>
    </div>
    
    
    </div>
    @else
    <div  class="card-footer">
    <button type="sumbit" value='' id="deny" class='btn btn-Danger' onclick="doAction( 3)">Cancel</button>
    <div><a href="{{ url('dynamic_pdf/pdf2') }}" class="btn btn-primary">Convert into PDF</a></div>
    <!--<button type="sumbit" value='' class='btn btn-primary'>Generate summary report</button>-->
    @endif
    </div>


    
@endif
</div>
</div>
@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script > 

function doAction(action) {
  if (action === 2) {
    
  } else {
    deny(action);
  }
}
function cbWrapper(data, funct){
    if($("#myForm", data).length > 0)
        top.location.href="/my-room-reservation";//redirection
    else
        funct(data);
}



function deny(action) {
  const swalWithBootstrapButtons = Swal.mixin({
    confirmButtonClass: 'btn btn-success',
    cancelButtonClass: 'btn btn-danger',
    buttonsStyling: false,
  })
  
  swalWithBootstrapButtons.fire({
    title: 'Deny Pending Reservation',
    text: "Are you sure you want to deny this reservation?",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes',
    cancelButtonText: 'No',
    reverseButtons: true
  }).then((result) => {
    if (result.value) {
      ajaxRequest(action);
      swalWithBootstrapButtons.fire(
        'Denied!',
        'Your pending reservation has been Denied.',
        'success',
        
      );
      
    } 
     })
     }
    


function ajaxRequest(action) {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  
  $.post('/my-room-reservation/save/{{$room_reservations->reservation_id}}', {action: action});
  window.location.replace('/my-room-reservation');
  console.log(action)

  
  //$('#reservation_' + resId).hide();
}
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.2.6/dist/sweetalert2.all.min.js"></script>  
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.2.6/dist/sweetalert2.min.css">

 