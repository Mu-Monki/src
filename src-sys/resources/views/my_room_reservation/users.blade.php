<table  class="container" border="1">

    <thead align='center'>
        <th>
        Reservation ID No.
        </th>
        <th>
        Room No.
        </th>
        <th>
        Name
        </th>
        <th>
        ID No.
        </th>
        <th>
        Date
        </th>
        <th>
        Timeslot
        </th>
        <th>
        Approval Status
        </th>
       
    </thead>
<tbody>

   @foreach($room_reservations as $room_reservation)
   <tr id="reservation_{{$room_reservation->reservation_id}}" align='center'>
   <td>
  <a href="{{route('my_room_reservation.list',['reservation_id' => $room_reservation->reservation_id])}}">{{$room_reservation->reservation_id}}</a> 
   </td>
   <td>
   {{$room_reservation->room->room_name}}
   </td>
   <td>
   {{$room_reservation->users->first_name}}
   </td>
   <td>
   {{$room_reservation->reservee_id}}
   </td>
   <td>
   {{$room_reservation->date_reserved}}
   </td>
   <td>
   {{$room_reservation->reservation_time_start}} - {{$room_reservation->reservation_time_end}}
   </td>
   <td>
   {{$room_reservation->reservation_status}}
   </td>
   <div id='test'></div>
   
    @endforeach
    <br><br><br>
    
    </tbody>
   
    
 </table>
 {{$room_reservations->render()}}
