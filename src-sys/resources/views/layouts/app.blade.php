<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SRC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" style="width: 24px;" style="height:24px;" type="image/png" href="./images/logover2.png">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SRC') }}</title>
     
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js'></script>
    <script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    
    <script src="{{ asset('js/wizard.js')}}" defer></script>
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('pagecss')
</head>
<body>

   <!-- <div class="container" id="app">-->
    @include('inc.messages')
   <div class="container" id="app">   
            @yield('content')
    </div>
    
    
</body>
</html>
