<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

// ADD '->middleware('auth')' to routes that need authentication

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'PagesController@home')->middleware('auth');
//Route::get('/room', 'PagesController@reserveRoom')->middleware('auth');
Route::post('/recaptcha', 'PagesController@recaptcha');
//Route::get('/preloader', 'PagesController@preloader');

//edit profile
Route::get('/usersprofile/{user}',  ['as' => 'users.edit', 'uses' => 'UserController@edit'])->middleware('auth');
Route::post('/usersprofile/{user}/update',  ['as' => 'users.update', 'uses' => 'UserController@update'])->middleware('auth');

//pending
//Route::get('pending/{room_reservations}',['uses' => 'PendingController@index', 'as' => 'pending.index']);
Route::get('/pending',  ['as' => 'pending.update', 'uses' => 'PendingController@update'])->middleware('auth'); 
Route::post('/pending/update', 'PendingController@doAction')->middleware('auth');
Route::get('/pending/search',  ['uses' =>'PendingController@search'])->name('pending.search')->middleware('auth');
Route::get('/pending/search_by_date',  ['uses' =>'PendingController@fetch_data'])->name('pending.sdate')->middleware('auth');
Route::get('/pending/list/{reservation_statuses}',['uses'=> 'PendingController@list','as' => 'pending.list'])->middleware('auth');
Route::post('/pending/save/{reservation_statuses}', ['uses' => 'PendingController@save', 'as' => 'pending.save'])->middleware('auth');

//my_room_reservation
Route::get('/my-room-reservation',['uses' =>'MyRoomReservationController@index'])->middleware('auth');
Route::get('/my-room-reservation/{room_reservations}',['uses' =>'MyRoomReservationController@list'])->name('my_room_reservation.list')->middleware('auth');
Route::get('/test/search', ['uses' =>'MyRoomReservationController@search'])->name('my_room_reservation.search')->middleware('auth');
Route::get('/test/search_by_date', ['uses' =>'MyRoomReservationController@fetch_data'])->name('my_room_reservation.sdate')->middleware('auth');
Route::post('/my-room-reservation/save/{room_reservations}', ['uses' => 'MyRoomReservationController@save', 'as' => 'my_room_reservation.save'])->middleware('auth');
Route::post('/my-room-reservation/update',['uses' => 'MyRoomReservationController@doAction'])->middleware('auth');


Route::get('/dynamic_pdf/pdf', 'MyRoomReservationController@pdf')->middleware('auth');
Route::get('/dynamic_pdf/pdf2', 'MyRoomReservationController@pdf2')->middleware('auth');

//RESERVE ROOM ROUTES
//Route::get('/room-reservations/search', 'SearchController@action')->name('room-reservations.action');
Route::get('/room-reservations/search', 'RoomReservationsController@search')->name('reserve.search');
Route::get('/room-reservations/update-table', 'RoomReservationsController@update_status')->name('reserve.update_status');
Route::resource('room-reservations', 'RoomReservationsController')->middleware('auth');
Route::resource('room-reservation-form', 'RoomReservationFormController')->middleware('auth');
Route::resource('room-reservation-form/{reservation}', 'RoomReservationFormController@index')->middleware('auth');

