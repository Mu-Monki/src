
function doAction(resId, action) {
  if (action === 2) {
    approve(resId, action);
  } else {
    deny(resId, action);
  }
}

function approve(resId, action) {
  const swalWithBootstrapButtons = Swal.mixin({
    confirmButtonClass: 'btn btn-success',
    cancelButtonClass: 'btn btn-danger',
    buttonsStyling: false,
  })
  
  swalWithBootstrapButtons.fire({
    title: 'Approve Pending Reservation',
    text: "Are you sure you want to approve this reservation?",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes',
    cancelButtonText: 'No',
    reverseButtons: true
  }).then((result) => {
    if (result.value) {
      ajaxRequest(resId, action);
      swalWithBootstrapButtons.fire(
        'Approved!',
        'Your pending reservation has been Approved.',
        'success'
      );
    } 
  })
}

function deny(resId, action) {
  const swalWithBootstrapButtons = Swal.mixin({
    confirmButtonClass: 'btn btn-success',
    cancelButtonClass: 'btn btn-danger',
    buttonsStyling: false,
  })
  
  swalWithBootstrapButtons.fire({
    title: 'Deny Pending Reservation',
    text: "Are you sure you want to deny this reservation?",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes',
    cancelButtonText: 'No',
    reverseButtons: true
  }).then((result) => {
    if (result.value) {
      ajaxRequest(resId, action);
      swalWithBootstrapButtons.fire(
        'Denied!',
        'Your pending reservation has been Denied.',
        'success'
      );
    } 
  })
}
function ajaxRequest(resId, action) {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  
  $.post('/pending/update', {resId: resId, action: action});
  $('#reservation_' + resId).hide();
}






$(document).ready(function() {

  

  //$('#search').change(function(){
  //fetch_data();

  function fetch_data(query = '') {
      $.ajax({
          url: '/pending/search',
          method: 'GET',
          data: {query:query},
          dataType: 'html',
          success:function(data) {
              console.log(data);
              //alert(data);
              $('tbody').html(data);
              //alert('success');
          }
      })
  }

  $(document).on('keyup', '#search', function(){
      let query = $(this).val();
      fetch_data(query);
  });
  

 
 
  



//$('button').click(function(){
    
  //fetch_data2();
  //var _token = $('input[name="_token"]').val();
  function fetch_data2(query = '',query2 = '') {
      $.ajax({
          url: '/pending/search_by_date',
          method: 'GET',
          data: {query:query,query2:query2},
          dataType: 'html',
          success:function(data) {
              console.log(data);
              //alert(data);
              $('tbody').html(data);
              //alert('success');
          }
      })
  } // Do something
   ;
  
   var start = document.getElementById('from_date');
   var end = document.getElementById('to_date');
   

   
   start.addEventListener('change', function() {
       if (start.value)
           end.min = start.value;
           var query = $('#from_date').val();
          var query2 = $('#to_date').val();
          if(query && query2){
          if(query <= query2 ){
           fetch_data2(query, query2); 
          }else{
            alert('message');
            $("#from_date").val(" ");
}
}
   }, false);
   
   end.addEventListener('change', function() {
    
    if (end.value)
           start.max = end.value;
              
           var query = $('#from_date').val();
          var query2 = $('#to_date').val();
          if(query<=query2){
            fetch_data2(query, query2); 
          }else{
            alert('message');
            $("#to_date").val(" ");
}
          
   }, false);
  
  
  
    


  
 // $.get('/pending/search_by_date', {from:from,to:to},function(data){console.log(data)
 // $('table').html(data)  });
  
//});

  

});
